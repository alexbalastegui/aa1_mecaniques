﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RasgosBehavior : MonoBehaviour {

    //en el enum guardo el nombre del rasgo y más abajo guardo cada sprite. 
    //Podría haber hecho un struct? sí, pero cuando me he dado cuenta ya era demasiado tarde.
    public enum rasgo
    {
        tresR,
        adictoTinta,
        adictoTrabajo,
        amorPropio,
        desastrillo,
        diogenes,
        esclavoModa,
        prestidigitacion,
        sentimentaloide,
        tsundere,
        vigorexia,
        max
    }
    public rasgo tipoRasgo;

    public Sprite tresR;
    public Sprite adictoTinta;
    public Sprite adictoTrabajo;
    public Sprite amorPropio;
    public Sprite desastrillo;
    public Sprite diogenes;
    public Sprite esclavoModa;
    public Sprite prestidigitacion;
    public Sprite sentimentaloide;
    public Sprite tsundere;
    public Sprite vigorexia;


    private BoxCollider2D hitBox;
    private SpriteRenderer rasgoSprite;
    private Camera cam;
    private Vector3 rasgoPosition;
    private bool rasgoSelected;

    // Use this for initialization
    void Start ()
    {
        hitBox = GetComponent<BoxCollider2D>();
        rasgoSprite = GetComponent<SpriteRenderer>();
        cam = FindObjectOfType<Camera>();
        rasgoSelected = false;

        tipoRasgo = (rasgo)Random.Range(0, (int)rasgo.max);

        switch (tipoRasgo)
        {
            case rasgo.tresR:
                rasgoSprite.sprite = tresR;
                break;
            case rasgo.adictoTinta:
                rasgoSprite.sprite = adictoTinta;
                break;
            case rasgo.adictoTrabajo:
                rasgoSprite.sprite = adictoTrabajo;
                break;
            case rasgo.amorPropio:
                rasgoSprite.sprite = amorPropio;
                break;
            case rasgo.desastrillo:
                rasgoSprite.sprite = desastrillo;
                break;
            case rasgo.diogenes:
                rasgoSprite.sprite = diogenes;
                break;
            case rasgo.esclavoModa:
                rasgoSprite.sprite = esclavoModa;
                break;
            case rasgo.prestidigitacion:
                rasgoSprite.sprite = prestidigitacion;
                break;
            case rasgo.sentimentaloide:
                rasgoSprite.sprite = sentimentaloide;
                break;
            case rasgo.tsundere:
                rasgoSprite.sprite = tsundere;
                break;
            case rasgo.vigorexia:
                rasgoSprite.sprite = vigorexia;
                break;
            default:
                break;
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            if (hitBox.bounds.Contains(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.gameObject.transform.position.z))))
            {
                if (rasgoSelected && RasgosController.rasgosControllerInstance.rasgosCounter <= 2)
                {
                    GetComponent<SpriteRenderer>().color = Color.white;
                    RasgosController.rasgosControllerInstance.rasgosCounter -= 1;
                    RasgosController.rasgosControllerInstance.rasgos.Remove(tipoRasgo);
                    rasgoSelected = false;
                }
                else if(!rasgoSelected && RasgosController.rasgosControllerInstance.rasgosCounter < 2)
                {
                    GetComponent<SpriteRenderer>().color = Color.green/*new Color(0.5f, 0.5f, 0.5f, 1f)*/;
                    RasgosController.rasgosControllerInstance.rasgosCounter += 1;
                    RasgosController.rasgosControllerInstance.rasgos.Add(tipoRasgo);
                    rasgoSelected = true;
                }
            }
        }
    }
}
