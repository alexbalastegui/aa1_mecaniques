﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUICombat : MonoBehaviour{

    public new Text name;
    public Text lvl; // String que se pinta en el HUD
    public Text happiness;// String que se pinta en el HUD
    public GameObject statsCanvas, optionsCanvas;
    public Button nameButton, optionsButton, deckButton;
    public SpriteRenderer bg1, bg2, bg3, bg4, bg5;
    public int sortingOrder = -3;

    public string lvlAux;// Aux para castear de int a string

    private string nameAux;
    
    private int happyLvl;//Numero de felicidad maxima

    private bool showStats, showOptions;


	// Use this for initialization
	void Start () {
        happyLvl = 0;
        nameAux = "Balillas";

        lvlAux = gameHandler.Instance.level.ToString();

        name.text = nameAux;
        lvl.text = lvlAux;

        nameButton.onClick.AddListener(TaskOnClickName);
        optionsButton.onClick.AddListener(TaskOnClickOptions);
        showStats = false;
        showOptions = false;

        statsCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        
    }

    void TaskOnClickName()
    {
        if (!showStats)
        {
            statsCanvas.SetActive(true);
            showStats = true;
        } 
        else
        {
            statsCanvas.SetActive(false);
            showStats = false;
        }
    }
    void TaskOnClickOptions()
    {
        if (!showOptions)
        {
            optionsCanvas.SetActive(true);
            showOptions = true;
        }
        else
        {
            optionsCanvas.SetActive(false);
            showOptions = false;
        }
       
    }
    // Update is called once per frame
    void Update () {
        happiness.text = gameHandler.Instance.hero.happiness.ToString();
	}
}
