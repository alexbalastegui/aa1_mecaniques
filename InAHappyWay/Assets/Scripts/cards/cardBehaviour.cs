﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cardBehaviour : MonoBehaviour {

    public enum type
    {
        cloth,
        book,
        paper,
        misc,
        sentimental,
        trash,
        max
    }

    public type typeCard = type.max;
    public int cost = 0;
    public int happiness = 0;
    public Sprite sprite;
    protected virtual void targetEffect(Protagonist hero, Enemy foe)
    {
        
    }

    protected virtual void generalEffect(Protagonist hero)
    {

    }
    public string description="This is the base card";

    public bool isTrueDamage = false;
    public bool isTemporal = false;
    public bool isConsumible = false;
    public bool isTarget = false;

    private BoxCollider2D hitBox;
    private Camera cam;

    public Vector3 initialPos;
    public Quaternion initialRot;

    private bool snapToMouse=false;
    private BoxCollider2D handArea;
    private SpriteRenderer imgur;

    public Protagonist instigator;
    public bool isEnemyTarget = true;

    protected TextMesh descriptionHolder;
    protected TextMesh costHolder;
    protected TextMesh happinessHolder;
    #region CardUtilities

    protected bool canBeUsed()
    {
        return instigator.mana >= cost;
    }

    protected void spendMana()
    {
        if (instigator.nigroMode)
        {
            instigator.Harm(cost, true);
            instigator.updateHealthUI();
        }
        else
        {
            instigator.mana -= cost;
            instigator.updateManaDisplay();
        }

        instigator.hand.Remove(gameObject);

    }

    protected void setDescription()
    {
        descriptionHolder.text = description;
    }

    protected void setHappiness(int newHappiness)
    {
        happiness = newHappiness;
        happinessHolder.text = happiness.ToString();
    }

    protected void setCost(int newCost)
    {
        cost = newCost;
        costHolder.text = cost.ToString();
    }

    #endregion

    virtual protected void Start()
    {
        //isEnemyTarget = true;

        initialPos = transform.position;
        initialRot = transform.rotation;

        hitBox = GetComponent<BoxCollider2D>();
        descriptionHolder = transform.Find("description").GetComponent<TextMesh>();
        costHolder = transform.Find("coste").GetComponent<TextMesh>();
        happinessHolder = transform.Find("felicidad").GetComponent<TextMesh>();

        cam = FindObjectOfType<Camera>();
        handArea = GameObject.Find("handArea").GetComponent<BoxCollider2D>();

        imgur = GetComponent<SpriteRenderer>();
        if (sprite != null)
            imgur.sprite = sprite;
    }

    virtual protected void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            if (gameHandler.Instance.playerTurn)
            {
                if (hitBox.bounds.Contains(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.gameObject.transform.position.z))))
                {
                    snapToMouse = true;
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (handArea.bounds.Contains(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.gameObject.transform.position.z))))
            {
                returnCardToHand();
            }
            else
            {
                //if card is enemyTarget
                if (snapToMouse)
                {
                    if (isEnemyTarget)
                    {
                        foreach (Enemy e in instigator.foes.ToArray())
                        {
                            Debug.Log("ho");
                            if (e.hitBox.bounds.Contains(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.gameObject.transform.position.z))))
                            {
                                if (canBeUsed())
                                {
                                    targetEffect(instigator, e);
                                    Destroy(gameObject);
                                }
                            }
                        }
                        returnCardToHand();
                    }
                    else
                    {
                        //return to hand
                        if (canBeUsed())
                        {
                            generalEffect(instigator);
                            Destroy(gameObject);
                        }
                    }
                }
                //dont destroy outside porque todas las cartas están comprobando esto gilipollas

            }
        }

        if (snapToMouse)
        {
            transform.position = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.gameObject.transform.position.z));

            if (isEnemyTarget)
            {
                foreach (Enemy e in instigator.foes.ToArray())
                {
                    if (e.hitBox.bounds.Contains(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.gameObject.transform.position.z))))
                        imgur.color = Color.yellow;
                    else
                        imgur.color = Color.white;
                }
            }
            else
            {
                if (handArea.bounds.Contains(cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -cam.gameObject.transform.position.z))))
                    imgur.color = Color.white;
                else
                    imgur.color = Color.yellow;
            }
        }
    }


    void returnCardToHand()
    {
        transform.position = initialPos;
        transform.rotation = initialRot;
        imgur.color = Color.white;
        snapToMouse = false;
    } 

}
