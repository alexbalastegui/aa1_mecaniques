﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosterDelLol : cardBehaviour {

    // Use this for initialization
    protected override void Start () {

        base.Start();

        typeCard = type.paper;
        cost = 2;
        happiness = 0;
        description = "Es un póster del lol\n luego probablemente \nte cause cancer";

        setDescription();
    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
	}


    protected override void targetEffect(Protagonist hero, Enemy foe)
    {
        foe.Harm(hero.strength-hero.happiness, false);
        base.spendMana();
    }
}
