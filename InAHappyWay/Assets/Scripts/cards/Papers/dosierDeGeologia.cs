﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dosierDeGeologia : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.paper;
        setCost(4);
        setHappiness(-2);
        description = "Ataca a todos los enemigos.\n +Felicidad/2 de daño.";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        foreach(Enemy e in hero.foes)
        {
            e.Harm(hero.strength + hero.happiness / 2, false);
        }
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
