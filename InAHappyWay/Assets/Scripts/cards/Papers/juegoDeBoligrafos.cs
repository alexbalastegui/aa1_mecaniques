﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class juegoDeBoligrafos : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.paper;
        setCost(2);
        setHappiness(1);
        description = "Ataca a un enemigo.\nRoba una carta.";

        setDescription();
    }

    protected override void targetEffect(Protagonist hero, Enemy foe)
    {
        foe.Harm(hero.strength, false);
        hero.DrawCards(1);
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
