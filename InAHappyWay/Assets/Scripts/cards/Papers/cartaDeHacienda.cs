﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cartaDeHacienda : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.paper;
        setCost(2);
        setHappiness(-3);
        description = "Otorga +5 de felicidad";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        hero.happiness += 5;
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
