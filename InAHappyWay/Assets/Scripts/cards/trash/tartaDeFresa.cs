﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tartaDeFresa : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.trash;
        cost = 2;
        happiness = 0;
        description = "Demasiado azúcar lleva esto\n para ser bueno";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
