﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class libroDeAutoayuda : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.trash;
        cost = 2;
        happiness = 0;
        description = "Es un libro de autoayuda./nNo hace nada";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
