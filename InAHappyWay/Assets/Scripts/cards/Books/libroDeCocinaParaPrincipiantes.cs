﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class libroDeCocinaParaPrincipiantes : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.book;
        setCost(1);
        setHappiness(0);
        description = "Añade una tarta de fresas \na tu pila de descartes. Recarga 3 de maná";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        base.spendMana();
        hero.mana += 3;
        if (hero.mana > hero.maxMana)
            hero.mana = hero.maxMana;
        hero.updateManaDisplay();
    }

    protected override void Update()
    {
        base.Update();
    }
}
