﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coleccionDeComics : cardBehaviour {

    // Use this for initialization
    
    protected override void Start()
    {
        base.Start();

        typeCard = type.book;
        setCost(3);
        setHappiness(5);
        description = "Ataca todos los enemigos.\n+3 de daño.";

        setDescription();
    }
    

    protected override void generalEffect(Protagonist hero)
    {
        foreach (Enemy e in hero.foes)
        {
            e.Harm(hero.strength, false);
        }
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
