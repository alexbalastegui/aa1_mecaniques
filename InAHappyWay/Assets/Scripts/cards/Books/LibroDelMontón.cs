﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LibroDelMontón : cardBehaviour {

	// Use this for initialization
	protected override void Start() {
        base.Start();

        typeCard = type.book;
        setCost(1);
        setHappiness(0);
        description = "Ataca a un enemigo";

        setDescription();
	}

    protected override void targetEffect(Protagonist hero, Enemy foe)
    {
        foe.Harm(hero.strength, false);
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
