﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class libroDeNigromancia : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.book;
        setCost(2);
        setHappiness(-3);
        description = "Las cartas que juegues durante este turno cuestan salud en lugar de maná.";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        hero.nigroMode = true;
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
