﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kitDePesas : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.misc;
        setCost(0);
        setHappiness(-1);
        description = "+2 de fuerza";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        instigator.strength += 2;
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
