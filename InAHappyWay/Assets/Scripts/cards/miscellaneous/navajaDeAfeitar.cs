﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class navajaDeAfeitar : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.misc;
        setCost(2);
        setHappiness(1);
        description = "Ataca dos veces a un enemigo.\nDaño verdadero.";

        setDescription();
    }

    protected override void targetEffect(Protagonist hero, Enemy foe)
    {
        foe.Harm(hero.strength, true);
        foe.Harm(hero.strength, true);
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
