﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fotografiaAntigua : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.sentimental;
        cost = 5;
        happiness = 10;
        description = "Causa 5 de daño + Felicidad/2(VERDADERO).\nRecupera 2 puntos de salud + Felicidad/2";

        setDescription();
    }

    protected override void targetEffect(Protagonist hero, Enemy foe)
    {
        foe.Harm(5+hero.happiness/2, true);
        hero.health += (2 + hero.happiness / 2);
        if (hero.health > hero.maxHealth)
            hero.health = hero.maxHealth;
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
