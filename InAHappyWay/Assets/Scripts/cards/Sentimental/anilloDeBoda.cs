﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anilloDeBoda : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        typeCard = type.sentimental;
        cost = 3;
        happiness = 6;
        description = "Roba 2 cartas.\nObtienes 20% de resistencia";

        setDescription();
    }

    protected override void generalEffect(Protagonist hero)
    {
        hero.DrawCards(2);
        hero.resistance += 20;
        if(hero.resistance>=100)
            hero.resistance = 99;

        hero.updateResistanceUI();
        base.spendMana();
    }

    protected override void Update()
    {
        base.Update();
    }
}
