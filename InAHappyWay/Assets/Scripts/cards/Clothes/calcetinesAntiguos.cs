﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class calcetinesAntiguos : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        typeCard = type.cloth;
        setCost(2);
        setHappiness(2);
        description = "Ataca a un enemigo.\n +Felicidad/3 de daño";

        setDescription();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    protected override void targetEffect(Protagonist hero, Enemy foe)
    {
        foe.Harm(hero.strength + hero.happiness/3, false);
        base.spendMana();
    }
}
