﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chandalDeMarca : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        typeCard = type.cloth;
        setCost(3);
        setHappiness(2);
        description = "+1 de fuerza al principio de cada turno.\n CONSUMIBLE";

        setDescription();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    protected override void generalEffect(Protagonist hero)
    {
        instigator.chandal = true;
        base.spendMana();
    }
}
