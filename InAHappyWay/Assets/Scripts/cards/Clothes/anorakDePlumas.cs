﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anorakDePlumas : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        typeCard = type.cloth;
        setCost(3);
        setHappiness(0);
        description = "Obtienes 10% de resistencia";

        setDescription();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    protected override void generalEffect(Protagonist hero)
    {
        instigator.resistance += 10;
        if (instigator.resistance >= 100)
            instigator.resistance = 99;

        instigator.updateResistanceUI();
        base.spendMana();
    }
}
