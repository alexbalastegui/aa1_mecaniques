﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camisetaDePulpFiction : cardBehaviour {

    // Use this for initialization
    protected override void Start()
    {

        base.Start();

        typeCard = type.cloth;
        setCost(1);
        setHappiness(1);
        description = "Ataca a un enemigo.\nSi felicidad >= 6 roba una carta";

        setDescription();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    protected override void targetEffect(Protagonist hero, Enemy foe)
    {
        foe.Harm(hero.strength, false);
        if (instigator.happiness >= 6)
            instigator.DrawCards(1);
        base.spendMana();
    }
}
