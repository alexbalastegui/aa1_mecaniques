﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RasgosController : MonoBehaviour {

    public int rasgosCounter;
    public static RasgosController rasgosControllerInstance;
    public Transform rasgoCard;

    RasgosBehavior rasgosBehavior;
    public List<RasgosBehavior.rasgo> rasgos = new List<RasgosBehavior.rasgo>();

    public Button readyButton;

    // Use this for initialization
    void Start()
    {
        rasgosControllerInstance = this;
        rasgosCounter = 0;
        Instantiate(rasgoCard, new Vector3(-6, 2, 0), Quaternion.identity);
        Instantiate(rasgoCard, new Vector3(0, 2, 0), Quaternion.identity);
        Instantiate(rasgoCard, new Vector3(6, 2, 0), Quaternion.identity);
        Instantiate(rasgoCard, new Vector3(-6, -2, 0), Quaternion.identity);
        Instantiate(rasgoCard, new Vector3(0, -2, 0), Quaternion.identity);
        Instantiate(rasgoCard, new Vector3(6, -2, 0), Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
        if (rasgosCounter < 2)
        {
            readyButton.interactable = false;
        }
        else
        {
            readyButton.interactable = true;
        }
    }


    public void ReadyRasgos()
    {
        Debug.Log("Has clickado ready");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
