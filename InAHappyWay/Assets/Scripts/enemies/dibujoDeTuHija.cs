﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dibujoDeTuHija : Enemy {

    // Use this for initialization
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    protected override IEnumerator Action()
    {
        yield return new WaitForSeconds(1.0f);
        gameHandler.Instance.hero.Harm(2, true);
    }
}
