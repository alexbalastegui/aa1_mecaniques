﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cajaDeHerramientas : Enemy {

	// Use this for initialization
	void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {
        base.Update();
	}

    protected override IEnumerator Action()
    {
        int index = Random.Range(0, 2);
        yield return new WaitForSeconds(1.0f);
        switch (index)
        {
            case 0:
                gameHandler.Instance.hero.Harm(2, false);
                break;
            case 1:
                gameHandler.Instance.hero.Harm(5, false);
                break;
        }
    }
}
