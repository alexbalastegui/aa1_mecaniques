﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : Character {

    public Collider2D hitBox;
    
    protected virtual IEnumerator Action()
    {
        yield return new WaitForSeconds(1.0f);
        gameHandler.Instance.hero.Harm(2, true);
    }

    // Use this for initialization
    protected override void Start() {
        base.Start();
        hitBox = GetComponent<BoxCollider2D>();
        
    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
	}

    public override void Die()
    {

        gameHandler.Instance.OnEnemyDeath(this);
        base.Die();
    }

    public override void BeginTurn()
    {
        StartCoroutine(Action());
        StartCoroutine(EndTurn());
    }

    virtual protected IEnumerator EndTurn()
    {
        yield return new WaitForSeconds(2.0f);
        gameHandler.Instance.EnemyEndTurn();
    }
}
