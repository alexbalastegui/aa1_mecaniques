﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPositionOnMap : MonoBehaviour {

    private Vector2 armario, salon, estudio, desvan, dormitorio;
    //HABRIA QUE PASARLE COMO PARAMETRO EL NIVEL EN EL QUE ESTAS!!
    private int level;
	// Use this for initialization
	void Start () {
        armario = new Vector2(7.18f, 0.27f);
        salon = new Vector2(-6.85f, 0.49f);
        estudio = new Vector2(-6.67f, -2.73f);
        desvan = new Vector2(-3.24f, -3.66f);
        dormitorio = new Vector2(2.96f, -1.29f);
        gameObject.transform.position = armario;
        level = 1;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp("r"))
        {
            level += 1;
        }
        if(level >= 1 && level <= 3)
        {
            gameObject.transform.position = armario;
        }
        if (level >= 4 && level <= 6)
        {
            gameObject.transform.position = salon;
        }
        if (level >= 7 && level <= 9)
        {
            gameObject.transform.position = estudio;
        }
        if (level >= 10 && level <= 12)
        {
            gameObject.transform.position = desvan;
        }
        if (level >= 13 && level <= 15)
        {
            gameObject.transform.position = dormitorio;
        }
        if (Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
