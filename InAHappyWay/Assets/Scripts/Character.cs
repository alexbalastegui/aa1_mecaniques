﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public int health = 0;
    public int maxHealth = 20;
    public int strength = 5;
    public int resistance = 0;
    
    private Image healthBar;
    private Text healthDisplay;
    private Text strengthDisplay;
    private Text resistanceDisplay;

    // Use this for initialization
    virtual protected void Start()
    {
        health = maxHealth;
        healthBar = transform.Find("Canvas").transform.Find("filledBar").GetComponent<Image>();
        healthDisplay = transform.Find("Canvas").transform.Find("filledBar").transform.Find("healthAmount").GetComponent<Text>();
        strengthDisplay = transform.Find("Canvas").transform.Find("fuerza").GetComponent<Text>();
        resistanceDisplay = transform.Find("Canvas").transform.Find("resistencia").GetComponent<Text>();

        updateHealthUI();
        updateStrengthUI();
        updateResistanceUI();
    }

    #region UIupdates
    public void updateHealthUI()
    {
        healthDisplay.text = health.ToString() + "/" + maxHealth.ToString();
        healthBar.fillAmount = ((float)health) / ((float)maxHealth);
    }

    public void updateStrengthUI()
    {
        strengthDisplay.text = strength.ToString();
    }

    public void updateResistanceUI()
    {
        resistanceDisplay.text = resistance.ToString() + "%";
    }
    #endregion
    public int Harm(int dmg, bool isTrueDmg)
    {
        float actualDmg;
        if (isTrueDmg || resistance == 0)
        {
            actualDmg = dmg;
        }
        else
        {
            actualDmg = ((float)dmg) * (resistance / 100.0f);
        }

        health -= ((int)actualDmg);
        updateHealthUI();

        if (health <= 0)
            Die();

        return ((int)actualDmg);
    }

    virtual public void Die()
    {
        Destroy(gameObject);
    }

    virtual public void BeginTurn()
    {

    }

    // Update is called once per frame
    virtual protected void Update()
    {

    }
}
