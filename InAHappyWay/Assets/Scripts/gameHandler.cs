﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameHandler : MonoBehaviour {

    public bool playerTurn;
    public Protagonist hero;
    public List<Enemy> foes;
    public Text turnDisplay;
    public Button passTurnButton;
    public ChangeUICombat combateUI;
    public Transform[] enemySpots;
    public List<Object> enemyList;

    public static gameHandler Instance { get; private set; }

    private int enemiesReady = 0;
    private int enemiesAlive = 0;
    private int enemiesToSpawn = 1;
    private bool doOnce;

    public int level = 1;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        doOnce = true;
        level = 1;
        playerTurn = true;
        hero = FindObjectOfType<Protagonist>();
        

        SpawnEnemy();
    }
	
	// Update is called once per frame
	void Update () {

		if(hero.health <= 0 && doOnce)
        {
            doOnce = false;
            SceneManager.LoadScene("defeatScene", LoadSceneMode.Single);
        }
	}

    public void EnemyEndTurn()
    {
        enemiesReady++;

        if(enemiesReady == foes.Count)
        {
            passTurn();
        }
    }

    public void passTurn()
    {
        enemiesReady = 0;
        playerTurn = !playerTurn;
        passTurnButton.interactable = playerTurn;
        if (hero.health <= 0)
            turnDisplay.text = "You Died";
        else
        {
            if (playerTurn)
            {
                turnDisplay.text = "Your turn";
                hero.BeginTurn();
            }
            else
            {
                turnDisplay.text = "Enemy turn";
                foreach (Enemy e in foes.ToArray())
                {
                    e.BeginTurn();
                }
            }
        }
    }

    public void OnEnemyDeath(Enemy g)
    {
        enemiesAlive--;

        hero.foes.Remove(g);
        foes.Remove(g);

        if (enemiesAlive <= 0)
        {
            passLevel();
        }
    }

    private void passLevel() {
        level += 1;
        combateUI.lvlAux = level.ToString();
        combateUI.lvl.text = combateUI.lvlAux;
        if (level >= 1 && level <= 3)
        {
            hero.maxMana = 3;
            enemiesToSpawn = 1;
            combateUI.bg1.enabled = true;
            combateUI.bg2.enabled = false;
            combateUI.bg3.enabled = false;
            combateUI.bg4.enabled = false;
            combateUI.bg5.enabled = false;
        }
        else if (level >= 4 && level <= 6)
        {
            hero.maxMana = 4;
            enemiesToSpawn = Random.Range(1, 2);
            combateUI.bg1.enabled = false;
            combateUI.bg2.enabled = true;
            combateUI.bg3.enabled = false;
            combateUI.bg4.enabled = false;
            combateUI.bg5.enabled = false;
        }
        else if (level >= 7 && level <= 9)
        {
            hero.maxMana = 5;
            enemiesToSpawn = Random.Range(1, 3);
            combateUI.bg1.enabled = false;
            combateUI.bg2.enabled = false;
            combateUI.bg3.enabled = true;
            combateUI.bg4.enabled = false;
            combateUI.bg5.enabled = false;
        }
        else if (level >= 10 && level <= 12)
        {
            hero.maxMana = 6;
            enemiesToSpawn = Random.Range(2, 3);
            combateUI.bg1.enabled = false;
            combateUI.bg2.enabled = false;
            combateUI.bg3.enabled = false;
            combateUI.bg4.enabled = true;
            combateUI.bg5.enabled = false;
        }
        else if (level >= 13 && level <= 15)
        {
            hero.maxMana = 7;
            enemiesToSpawn = Random.Range(2, 4);
            combateUI.bg1.enabled = false;
            combateUI.bg2.enabled = false;
            combateUI.bg3.enabled = false;
            combateUI.bg4.enabled = false;
            combateUI.bg5.enabled = true;
        }
        else if(level > 15)
        {
            SceneManager.LoadScene("winScene", LoadSceneMode.Single);
        }
        for (int i = 0; i < enemiesToSpawn; ++i)
        {
            SpawnEnemy();
        }
        hero.mana = hero.maxMana;
        hero.updateManaDisplay();
        hero.health = hero.maxHealth;
        hero.updateHealthUI();
    }

    private void SpawnEnemy()
    {

        GameObject tempEnemy = Instantiate(enemyList[Random.Range(0, enemyList.Count-1)]) as GameObject;
        tempEnemy.transform.position = enemySpots[foes.Count].position;
        hero.foes.Add(tempEnemy.GetComponent<Enemy>());
        foes.Add(tempEnemy.GetComponent<Enemy>());
        enemiesAlive++;

    }
}
