﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Protagonist : Character {
    
    public int happiness;
    public int mana = 0;
    public int maxMana = 3;

    public List<Enemy> foes;
    public List<GameObject> hand;
    public Transform[] handSpots;
    public List<Object> persistentDeck;
    public List<Object> temporalDeck;
    private Text manaDisplay;

    public bool nigroMode;
    public bool chandal;


    // Use this for initialization
    protected override void Start () {
        base.Start();
        strength = 500;
        nigroMode = false;
        chandal = false;

        temporalDeck = persistentDeck;
            
        manaDisplay = transform.Find("Canvas").transform.Find("mana").GetComponent<Text>();
        updateHealthUI();
        #region Setup

        DrawCards(3);

        nigroMode = false;

        if (chandal)
        {
            strength++;
            updateStrengthUI();
        }

        mana = maxMana;
        recalcHappiness();
        #endregion
    }

    #region UIupdates

    public void updateManaDisplay()
    {
        manaDisplay.text = mana.ToString() + "/" + maxMana.ToString();
    }
    
    #endregion

    public override void BeginTurn()
    {
        DrawCards(1);

        nigroMode = false;

        if (chandal)
        {
            strength++;
            updateStrengthUI();
        }

        mana = maxMana;
        updateManaDisplay();
    }

    private void recalcHappiness()
    {
        happiness = 0;
        foreach(Object g in temporalDeck.ToArray())
        {
            happiness += (g as GameObject).GetComponent<cardBehaviour>().happiness;
        }
        Debug.Log(happiness);
    }

    public void DrawCards(int amount)
    {
        for (int i = 0; i < amount; ++i)
        {           
            if (hand.Count < 5)
            {
                GameObject tempCard = Instantiate(temporalDeck[i]) as GameObject;
                tempCard.transform.position = handSpots[hand.Count].position;
                tempCard.GetComponent<cardBehaviour>().instigator = this;
                hand.Add(tempCard);
                Debug.Log(hand.Count);
            }

            temporalDeck.RemoveAt(i);
        }
        int u = 0;
        foreach(GameObject g in hand.ToArray())
        {
            g.transform.position = handSpots[u].position;
            g.GetComponent<cardBehaviour>().initialPos = handSpots[u].position;
            g.GetComponent<cardBehaviour>().initialRot = handSpots[u++].rotation;
        }
    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
		
	}
}
